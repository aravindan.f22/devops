Rails.application.routes.draw do
  get "/ping", to: proc { [200, {}, ['Hello World']] }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
